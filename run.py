import yaml
import json
import logging
import boto3
import sys
import argparse
import requests
import datetime

parser = argparse.ArgumentParser(
    description="SNOW integration bot", prog="snowBot")
parser.add_argument("-c", "--config", help="configuration file to load")
parser.add_argument("-d", "--debug",
                    action="store_true", dest="debug",
                    help='enable debug logging')
args = parser.parse_args()

logger = logging.getLogger('AlertManagerWorker')
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
if args.debug:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

config = yaml.load(open(args.config))

headers = {"Content-Type": "application/json",
           "Accept": "application/json"}

STATE_NEW = '1'
STATE_ACTIVE = '2'
STATE_HOLD = '3'
STATE_AWAITING = '-10'
STATE_RESOLVED = '6'
STATE_CLOSED = '7'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
P1s = []
P2s = []
P3s = []
P4s = []
P1 = '1'
P2 = '2'
P3 = '3'
P4 = '4'


def breached(inc, tickets, sla, priority):
    if inc['incident_state'] == STATE_ACTIVE and inc['priority'] == priority:
        last_update = datetime.datetime.strptime(
            inc['sys_updated_on'], DATETIME_FORMAT)
        diff = last_update + datetime.timedelta(seconds=sla)
        if datetime.datetime.now() > diff:
            tickets.append(inc)


def main():
    # get company name
    company = requests.get('{}/api/now/table/core_company?sysparm_query=sys_id={}'.format(
        config['SNOW_URL'], config['COMPANY_ID']),
        auth=(config['SNOW_USER'], config['SNOW_PASS']), headers=headers)
    if company.status_code != 200:
        logger.info("Failed to fetch from SNOW...{} {}".format(
            company.status_code, company.text))
    else:
        company = company.json()
        company_name = company['result'][0]['name']

    # fetch all incidents for customer
    incidents = requests.get('{}/api/now/table/incident?sysparm_query=company={}'.format(
        config['SNOW_URL'], config['COMPANY_ID']),
        auth=(config['SNOW_USER'], config['SNOW_PASS']), headers=headers)
    if incidents.status_code != 200:
        logger.info("Failed to fetch from SNOW...{} {}".format(
            incidents.status_code, incidents.text))
    else:
        response = incidents.json()
        for inc in response['result']:
            if inc['incident_state'] == STATE_ACTIVE and inc['priority'] == P1:
                breached(inc, P1s, config['P1_SLA'], P1)
            elif inc['incident_state'] == STATE_ACTIVE and inc['priority'] == P2:
                breached(inc, P2s, config['P2_SLA'], P2)
            elif inc['incident_state'] == STATE_ACTIVE and inc['priority'] == P3:
                breached(inc, P3s, config['P3_SLA'], P3)
            elif inc['incident_state'] == STATE_ACTIVE and inc['priority'] == P4:
                breached(inc, P4s, config['P4_SLA'], P4)

    print("{} SLA Breached P1s : {}".format(company_name, len(P1s)))
    print("{} SLA Breached P2s : {}".format(company_name, len(P2s)))
    print("{} SLA Breached P3s : {}".format(company_name, len(P3s)))
    print("{} SLA Breached P4s : {}".format(company_name, len(P4s)))


if __name__ == '__main__':
    main()
